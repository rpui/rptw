///
    This file is part of Rptw project.

    Rptw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rptw.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",   
    "dijit/layout/ContentPane", 
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/form/ToggleButton",
    "app/twbase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        ContentPane,
        TextBox,
        Button,
        ToggleButton,
        twbase) ->







        declare "tw_switch", [TWBase],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this.sendbutton = false
                this.ignore = false




            postCreate:() ->
                this.nameContent = new ContentPane({ class:"tw_switch_nameContent" })
                this.statusContent = new ContentPane({ class:"tw_switch_statusContent" })
                this.domNode.appendChild(this.statusContent.domNode)
                this.domNode.appendChild(this.nameContent.domNode)
                
                this.titleDom = domConstruct.create(
                    "label", {innerHTML:this.defs.title})
                
                this.nameContent.domNode.appendChild(this.titleDom)
                #label = domConstruct.create("label", 
                #    {for:this.id+"cpubar", innerHTML:this.twdata.i18n.Cpu_Load})
                this.switch = new ToggleButton({
                    id:this.id+"switch",         
                    checked: false,
                    value: false,
                    onClick: lang.hitch(this, "click"),
                    onChange: lang.hitch(this, "change"),
                    label: this.twdata.i18n.Off})

                this.statusContent.domNode.appendChild(this.switch.domNode)
                this.switch.startup()




            click:(sw) ->
                this.ignore = true
                this.tw_change()




            change:(v) ->
                if v == true
                    this.switch.set('label', this.twdata.i18n.On)
                else
                    this.switch.set('label', this.twdata.i18n.Off)

                    


            update:(data)->
                if this.is_writing()
                    return
                if this.ignore
                    this.ignore = false
                    return
                try
                    state = data[this.defs.variables.status]
                    status = state.status
                    if status == "on"
                        this.switch.set("checked", true)
                    else
                        this.switch.set("checked", false)
                catch e
                    log(e)
                


            
            data: () ->
                data = {}
                data[this.defs.variables.status]  = this.switch.checked
                return(data)
