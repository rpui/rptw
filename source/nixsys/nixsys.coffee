///
    This file is part of Rptw project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",    
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/ProgressBar",
    "app/twbase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        TextBox,
        Button,
        ProgressBar,
        twbase) ->







        declare "tw_nixsys", [TWBase],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                #console.log (this.defs)
                #console.log (this.task)
                #console.log (this.twdata)





            postCreate:() ->
                if this.defs.title != ""
                    this.domNode.appendChild(domConstruct.create(
                        "label", {innerHTML:this.defs.title}))
                    this.domNode.appendChild(domConstruct.create("br"))

                cpulabel = domConstruct.create("label", 
                    {for:this.id+"cpubar", innerHTML:this.twdata.i18n.Cpu_Load})
                this.cpu_load = new ProgressBar({
                    id: this.id+"cpubar"})

                this.domNode.appendChild(cpulabel)
                this.domNode.appendChild(domConstruct.create("br"))
                this.domNode.appendChild(this.cpu_load.domNode)
                this.cpu_load.startup()

                ramlabel = domConstruct.create("label", 
                    {for:this.id+"rambar", innerHTML:this.twdata.i18n.Ram_Load})
                this.ram_load = new ProgressBar({
                    id: this.id+"rambar"})

                this.domNode.appendChild(domConstruct.create("br"))
                this.domNode.appendChild(ramlabel)
                this.domNode.appendChild(domConstruct.create("br"))
                this.domNode.appendChild(this.ram_load.domNode)
                this.ram_load.startup()
                


            update:(data)->
                cpuload = this.defs.variables.cpu_usage
                ramfree = this.defs.variables.ram_free
                ramtotal = this.defs.variables.ram_total
                ramload = 100-((100/Number(data[ramtotal]))*Number(data[ramfree]))
                this.cpu_load.set("value", Number(data[cpuload]))
                this.ram_load.set("value", ramload)