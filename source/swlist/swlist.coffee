///
    This file is part of Rptw project.

    Rptw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rptw.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",   
    "dijit/layout/ContentPane", 
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/form/ToggleButton",
    "app/twbase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        ContentPane,
        TextBox,
        Button,
        ToggleButton,
        twbase) ->







        declare "tw_swlist", [TWBase],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this.sendbutton = false
                this.ignore = false




            postCreate:() ->
                #console.log(this.twdata)
                names = this.defs.constants.names.split(",")
                titles = this.defs.constants.titles.split(",")
                ontxts = this.defs.constants.ontxts.split(",")                
                offtxts = this.defs.constants.offtxts.split(",")
                if this.defs.constants.mode == "row"
                    moderow = true
                else
                    moderow = false
                this.table = []

                for i in Object.keys(names)

                    if moderow
                        row = new ContentPane({ class:"tw_swlist_ContentRow" })
                    else
                        row = new ContentPane({ class:"tw_swlist_ContentNext" })

                    statusContent = new ContentPane({ class:"tw_swlist_statusContent" })
                    row.domNode.appendChild(statusContent.domNode)

                    try
                        if titles[i].length > 0
                            titleDom = domConstruct.create(
                                "label", {innerHTML:titles[i], class:"tw_swlist_Label"})
                            nameContent = new ContentPane({ class:"tw_swlist_nameContent" })
                            nameContent.domNode.appendChild(titleDom)
                            row.domNode.appendChild(nameContent.domNode)
                    catch
                        null
                    
                    this.domNode.appendChild(row.domNode)
                    

                    if this.twdata.i18n.hasOwnProperty(ontxts[i])
                        ontxt = this.twdata.i18n[ontxts[i]]
                    else
                        ontxt = ontxts[i]
                    if this.twdata.i18n.hasOwnProperty(offtxts[i])
                        offtxt = this.twdata.i18n[offtxts[i]]
                    else
                        offtxt = offtxts[i] 

                    sw = new ToggleButton({
                        id:this.id+names[i],
                        class: "tw_swlist_Button",
                        swname: names[i],
                        ontxt: ontxt, 
                        offtxt: offtxt,
                        checked: false,
                        value: false,
                        onClick: lang.hitch(this, "click"),
                        onChange: this.change,
                        label: offtxt})

                    this.table.push({
                        "name":names[i], 
                        "dom":sw})

                    statusContent.domNode.appendChild(sw.domNode)
                    sw.startup()





            click:(sw) ->
                this.ignore = true
                this.tw_change()




            change:(v) ->
                if v == true
                    this.set('label', this.ontxt)
                else
                    this.set('label', this.offtxt)

                    


            update:(data)->
                if this.is_writing()
                    return
                if this.ignore
                    this.ignore = false
                    return
                try
                    for sw in this.table
                        if data.hasOwnProperty(sw.name)
                            if data[sw.name] == "1"
                                sw.dom.set("checked", true)
                            else
                                sw.dom.set("checked", false)
                catch e
                    console.log(e)
                


            
            data: () ->
                data = {}
                for sw in this.table
                    if sw.dom.checked
                        data[sw.name] = "1"
                    else
                        data[sw.name] = "0"
                return(data)
